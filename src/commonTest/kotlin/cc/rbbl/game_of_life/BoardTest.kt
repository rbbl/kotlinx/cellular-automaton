package cc.rbbl.game_of_life

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BoardTest {
    @Test
    fun generateGOLBoardSeedConsistencyTest1() {
        val board = generateGOLBoard(10, 10)
        val board2 = generateGOLBoard(10, 10)
        assertTrue { board.contentDeepEquals(board2) }
    }

    @Test
    fun generateGOLBoardSeedConsistencyTest2() {
        val board = generateGOLBoard(10, 10, 420)
        val board2 = generateGOLBoard(10, 10, 420)
        assertTrue { board.contentDeepEquals(board2) }
    }

    @Test
    fun generateGOLBoardStringSeedConsistencyTest() {
        val board = generateGOLBoard(10, 10, "henlo")
        val board2 = generateGOLBoard(10, 10, "henlo")
        assertTrue { board.contentDeepEquals(board2) }
    }

    private val threeByThreeBoard: Array<Array<Boolean>> = arrayOf(
        arrayOf(true, true, true, true),
        arrayOf(true, false, false, false),
        arrayOf(true, false, false, false),
        arrayOf(true, false, false, false)
    )

    @Test
    fun getCellStartTorusTest() {
        val board = Board(threeByThreeBoard)
        val cell = board.getCell(0, 0)
        assertEquals(4, cell.neighbours.getTrueCount())
    }

    @Test
    fun getCellEndTorusTest() {
        val board = Board(threeByThreeBoard)
        val cell = board.getCell(3, 3)
        assertEquals(5, cell.neighbours.getTrueCount())
    }

    @Test
    fun getCellStartBorderTest() {
        val board = Board(threeByThreeBoard, false)
        val cell = board.getCell(0, 0)
        assertEquals(2, cell.neighbours.getTrueCount())
    }

    @Test
    fun getCellEndBorderTest() {
        val board = Board(threeByThreeBoard, false)
        val cell = board.getCell(3, 3)
        assertEquals(0, cell.neighbours.getTrueCount())
    }

    private val smallestStaticBoard: Array<Array<Boolean>> = arrayOf(
        arrayOf(true, true),
        arrayOf(true, true)
    )

    @Test
    fun staticGenerationPreviewTest() {
        val board = Board(smallestStaticBoard, false)
        assertTrue { board.previewNextGeneration().contentDeepEquals(smallestStaticBoard) }
    }

    private val threeByThreeStaticBoard: Array<Array<Boolean>> = arrayOf(
        arrayOf(false, true, false),
        arrayOf(true, false, true),
        arrayOf(false, true, false)
    )

    @Test
    fun staticGenerationPreviewTest2() {
        val board = Board(threeByThreeStaticBoard, false)
        assertTrue { board.previewNextGeneration().contentDeepEquals(threeByThreeStaticBoard) }
    }

    private val blinkerBoard1: Array<Array<Boolean>> = arrayOf(
        arrayOf(false, false, false),
        arrayOf(true, true, true),
        arrayOf(false, false, false)
    )

    private val blinkerBoard2: Array<Array<Boolean>> = arrayOf(
        arrayOf(false, true, false),
        arrayOf(false, true, false),
        arrayOf(false, true, false)
    )

    @Test
    fun getCellBlinkerBorderTest() {
        val board = Board(blinkerBoard1, false)
        assertEquals(2, board.getCell(0, 0).neighbours.getTrueCount())
        assertEquals(3, board.getCell(0, 1).neighbours.getTrueCount())
        assertEquals(2, board.getCell(0, 2).neighbours.getTrueCount())
        assertEquals(1, board.getCell(1, 0).neighbours.getTrueCount())
        assertEquals(2, board.getCell(1, 1).neighbours.getTrueCount())
        assertEquals(1, board.getCell(1, 2).neighbours.getTrueCount())
        assertEquals(2, board.getCell(2, 0).neighbours.getTrueCount())
        assertEquals(3, board.getCell(2, 1).neighbours.getTrueCount())
        assertEquals(2, board.getCell(2, 2).neighbours.getTrueCount())
    }

    @Test
    fun blinkerGenerationPreviewTest() {
        val board = Board(blinkerBoard1, false)
        assertTrue { board.previewNextGeneration().contentDeepEquals(blinkerBoard2) }
    }

    private val octagonBoard: Array<Array<Boolean>> = arrayOf(
        arrayOf(false, false, false, true, true, false, false, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, true, false, false, false, false, true, false),
        arrayOf(true, false, false, false, false, false, false, true),
        arrayOf(true, false, false, false, false, false, false, true),
        arrayOf(false, true, false, false, false, false, true, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, false, false, true, true, false, false, false)
    )

    private val octagonBoard3: Array<Array<Boolean>> = arrayOf(
        arrayOf(false, false, false, false, false, false, false, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, true, false, true, true, false, true, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, true, false, true, true, false, true, false),
        arrayOf(false, false, true, false, false, true, false, false),
        arrayOf(false, false, false, false, false, false, false, false)
    )

    @Test
    fun simulationBlinkerTest() {
        val board = Board(blinkerBoard1, false)
        assertTrue { board.simulateGenerations(2, false).contentDeepEquals(blinkerBoard1) }
    }

    @Test
    fun simulationOctagonTest() {
        val board = Board(octagonBoard, false)
        assertTrue { board.simulateGenerations(5, false).contentDeepEquals(octagonBoard) }
        assertTrue { board.cells.contentDeepEquals(octagonBoard) }
    }

    @Test
    fun simulationApplicationTest() {
        val board = Board(octagonBoard, false)
        assertTrue { board.simulateGenerations(3, false).contentDeepEquals(octagonBoard3) }
        assertTrue { board.cells.contentDeepEquals(octagonBoard) }
    }
}