package cc.rbbl.game_of_life

import kotlin.test.Test
import kotlin.test.assertEquals

class CellTest {
    @Test
    fun trueCountTest() {
        assertEquals(3, arrayOf(true, false, false, true, true, false).getTrueCount())
    }

    @Test
    fun cellEvolutionTest() {
        val testCases = mapOf<Cell, Boolean>(
            Cell(true, 0, 0, arrayOf(false)) to false,
            Cell(false, 0, 0, arrayOf(false)) to false,
            Cell(true, 0, 0, arrayOf(true)) to false,
            Cell(false, 0, 0, arrayOf(true)) to false,
            Cell(true, 0, 0, arrayOf(true, true)) to true,
            Cell(false, 0, 0, arrayOf(true, true)) to false,
            Cell(true, 0, 0, arrayOf(true, true, true)) to true,
            Cell(false, 0, 0, arrayOf(true, true, true)) to true,
            Cell(true, 0, 0, arrayOf(true, true, true, true)) to false,
            Cell(false, 0, 0, arrayOf(true, true, true, true)) to false,
            Cell(true, 0, 0, arrayOf(true, true, true, true, true, true, true, true)) to false,
            Cell(false, 0, 0, arrayOf(true, true, true, true, true, true, true, true)) to false,
        )
        for (case in testCases) {
            assertEquals(case.value, case.key.valueInNextGeneration())
        }
    }
}