package cc.rbbl.game_of_life

/**
 * A single Cell of the Cell Automaton
 */
data class Cell(val value: Boolean, val x: Int, val y: Int, val neighbours: Array<Boolean>) {
    /**
     * Returns the liveliness Value of this the in the next Generation based on Cells [neighbours]
     *
     * @return liveliness Value
     */
    fun valueInNextGeneration(): Boolean {
        val neighbourCount = neighbours.getTrueCount()
        return when {
            neighbourCount == 3 -> true
            neighbourCount < 2 -> false
            neighbourCount > 3 -> false
            else -> value
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Cell

        if (value != other.value) return false
        if (x != other.x) return false
        if (y != other.y) return false
        if (!neighbours.contentEquals(other.neighbours)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = value.hashCode()
        result = 31 * result + x
        result = 31 * result + y
        result = 31 * result + neighbours.contentHashCode()
        return result
    }
}


/**
 * return a Count of how many true Booleans the Array does contain
 *
 * @return Count
 */
fun Array<Boolean>.getTrueCount(): Int = this.fold(0) { acc, b ->
    if (b) {
        acc + 1
    } else {
        acc
    }
}