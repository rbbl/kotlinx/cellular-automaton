package cc.rbbl.game_of_life

import kotlin.random.Random

/**
 * A Board used to simulate a Cell Automaton
 */
class Board(val cells: Array<Array<Boolean>>, val torus: Boolean = true) {

    fun getCell(x: Int, y: Int): Cell = getCell(x, y, cells)

    private fun getCell(x: Int, y: Int, cells: Array<Array<Boolean>>): Cell =
        Cell(cells[x][y], x, y, getNeighbors(x, y, cells))

    private fun getNeighbors(x: Int, y: Int, cells: Array<Array<Boolean>>): Array<Boolean> {
        val neighbors: Array<Boolean> = Array(8) { false }
        var counter = 0
        for (xIterator in x - 1..x + 1) {
            yLoop@ for (yIterator in y - 1..y + 1) {
                if (xIterator == x && yIterator == y) {
                    continue@yLoop
                }
                neighbors[counter] = if (torus) {
                    val xToUse: Int = if (xIterator > cells.lastIndex) {
                        0
                    } else if (xIterator < 0) {
                        cells.lastIndex
                    } else {
                        xIterator
                    }
                    val yToUse: Int = if (yIterator > cells[x].lastIndex) {
                        0
                    } else if (yIterator < 0) {
                        cells[x].lastIndex
                    } else {
                        yIterator
                    }
                    cells[xToUse][yToUse]
                } else {
                    try {
                        cells[xIterator][yIterator]
                    } catch (e: Throwable) {
                        false
                    }
                }
                counter++
            }
        }
        return neighbors
    }

    fun previewNextGeneration() = previewNextGeneration(cells)

    private fun previewNextGeneration(cells: Array<Array<Boolean>>): Array<Array<Boolean>> {
        val nextGeneration = cells.deepCopy()
        for (x in cells.indices) {
            for (y in cells[x].indices) {
                nextGeneration[x][y] = getCell(x, y, cells).valueInNextGeneration()
            }
        }
        return nextGeneration
    }

    fun simulateGenerations(
        generations: Int,
        applyToBoard: Boolean = true,
        generationObserver: (currentGeneration: Int, cells: Array<Array<Boolean>>) -> Unit = { _, _ -> }
    ): Array<Array<Boolean>> {
        var simulatedGeneration = cells.deepCopy()
        for (i in 0 until   generations) {
            generationObserver(i, simulatedGeneration)
            simulatedGeneration = previewNextGeneration(simulatedGeneration)
        }
        if (applyToBoard) {
            simulatedGeneration.copyInto(cells)
        }
        return simulatedGeneration
    }
}

fun generateGOLBoard(width: Int, height: Int, seed: Long = 420): Array<Array<Boolean>> {
    val board = Array(width) { Array(height) { false } }
    val random = Random(seed)
    for (x in 0 until width) {
        for (y in 0 until height) {
            board[x][y] = random.nextBoolean()
        }
    }
    return board
}

fun generateGOLBoard(width: Int, height: Int, seed: String): Array<Array<Boolean>> {
    return generateGOLBoard(width, height, seed.fold(0) { acc, char -> acc + (char.code) })
}

fun Array<Array<Boolean>>.deepCopy(): Array<Array<Boolean>> {
    val list = mutableListOf<Array<Boolean>>()
    for (column in this) {
        val newColumn = Array(column.size) { false }
        list.add(column.copyInto(newColumn))
    }
    return list.toTypedArray()
}