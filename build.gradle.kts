plugins {
    kotlin("multiplatform") version "1.6.21"
    id("maven-publish")
    signing
}

group = "cc.rbbl.game-of-life"
version = System.getenv("BUILD_VERSION") ?: "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    js(BOTH) {
        browser {
            testTask {
                useKarma {
                    if (System.getProperty("user.name") == "root") {
                        useChromeHeadlessNoSandbox()
                    } else {
                        useChromeHeadless()
                    }
                    useFirefoxHeadless()
                }
            }
            commonWebpackConfig {
                cssSupport.enabled = true
            }
        }
    }

    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }


    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting
        val jsTest by getting
        val nativeMain by getting
        val nativeTest by getting
    }
}

val javadocJar by tasks.registering(Jar::class) {
    archiveClassifier.set("javadoc")
}

publishing {
    publications.withType<MavenPublication> {
        artifact(javadocJar)
        pom {
            name.set("Game of Life Library")
            description.set("A small Library to generate and simulate the Game of Life")
            url.set("https://www.rbbl.cc/projects")
            licenses {
                license {
                    name.set("MIT License")
                    url.set("https://gitlab.com/rbbl/game-of-life/game-of-life-lib/-/raw/master/LICENSE")
                }
            }
            developers {
                developer {
                    id.set("rbbl-dev")
                    name.set("rbbl-dev")
                    email.set("dev@rbbl.cc")
                }
            }
            scm {
                url.set("https://gitlab.com/rbbl/game-of-life/game-of-life-lib")
            }
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/35454895/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
        maven {
            name = "Snapshot"
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials(PasswordCredentials::class)
        }
        maven {
            name = "Central"
            url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials(PasswordCredentials::class)
        }
    }
}

signing {
    val signingKey: String? by project
    val signingPassword: String? by project
    useInMemoryPgpKeys(signingKey, signingPassword)
    setRequired({
        gradle.taskGraph.allTasks.filter { it.name.endsWith("ToCentralRepository") }.isNotEmpty()
    })
    sign(publishing.publications)
}